# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 14:09:38 2021

@author: ruairi
"""

import pandas as pd
import numpy as np
#from scipy import stats
from datetime import datetime
from datetime import timedelta
#from sklearn import preprocessing
#from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
import matplotlib
import matplotlib.pyplot as plt
import pickle
import statsmodels.api as sm
from statsmodels.regression.rolling import RollingOLS
from sqlalchemy import create_engine
import seaborn as sns
sns.set_style('darkgrid')
pd.plotting.register_matplotlib_converters()
from matplotlib.backends.backend_pdf import PdfPages
import statsmodels.api as sm
from statsmodels.tsa.stattools import grangercausalitytests



def make_eng():
    import sqlalchemy as sq
    host = '127.0.0.1'
    db = 'tiggy_prod'
    port = 5432
    user = 'tiggy_ro'
    password ='INSERT PASSWORD'
    return create_engine(f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{db}')


def query_construct(table, coin, metric):
    return "SELECT p.as_of, p.symbol, p.value as price, m.as_of as m_as_of, m.symbol as m_symbol, m.metric as m_metric, m.value as m_value  from (SELECT * from " + str(table) + " WHERE symbol='" + str(coin) + "' AND metric = '"+str(metric)+"' ) m INNER JOIN (SELECT as_of, symbol, metric, value from glassnode_market where symbol = '"+str(coin)+"' AND metric = 'price_usd') p on m.as_of = p.as_of"
    

def simple_corr_strat(df, difference=1):
    
    df = df.diff(difference)
    
    for col in df.columns:
        df.loc[:, 'sig_' + str(col)] = df.loc[:,col].apply(lambda x: 1 if x>0 else .5)
        
    return df

def corr_portfolio(coin, table, metric, difference=[1,2,5,10,30], shift=[1,2,3,4,-4,-3,-2,-1], rolling_win=30):
    
    #Calculate portfolio return of modified holdings based on one day lagged data for a selction of differencing   
    query = query_construct(table,coin,metric)
    
    df = pd.read_sql(query, make_eng())
    df = df.set_index('as_of')
    df = df.loc[:, ['price', 'm_value']]
    df = df.rename(columns={'m_value':'today'})


    for i in shift:
        df['lag_' + str(i)] = df.today.shift(i)
    

    with PdfPages('graphs/strat_' + str(coin) + '_' + str(metric)+ '.pdf') as pdf:        
        for diff in difference:
            df_diff = simple_corr_strat(df, diff)
            df_diff['real'] =1    
            strat_return = (1+df_diff.mul(df.loc[:, 'price'].pct_change(), axis=0)).cumprod()
            strat_return['ratio'] = strat_return['sig_lag_1'] / strat_return['real']
        
            fig = plt.figure()
            strat_return.loc[:, ['real','sig_lag_1', 'sig_lag_2', 'sig_lag_3', 'sig_lag_-1','sig_lag_-2', 'sig_lag_-3']].plot()
            plt.title(str(coin) + ' 1_day_change ' + str(metric)+ ' differenced ' + str(diff) +' day', fontsize=8)
            pdf.savefig(dpi=300)
            plt.close()
            np.log(strat_return.loc[:, ['real','sig_lag_1', 'sig_lag_2', 'sig_lag_3', 'sig_lag_-1','sig_lag_-2', 'sig_lag_-3']]).plot()
            plt.title(str(coin) + ' log_1_day_change '+ str(metric) + ' differenced '+ str(diff)+' day', fontsize=8)
            pdf.savefig(dpi=300)
            plt.close()
            strat_return['ratio'].plot()
            plt.title(str(coin) + ' lag_1 ratio to buy & hold' + str(metric)+ ' differenced ' + str(diff) +' day', fontsize=8)
            pdf.savefig()
            plt.close()

def calc_corr(coin, table, metric, difference=1, shift=[1,2,3,4,-4,-3,-2,-1], rolling_win=30, split=4):
    #Calculate correlations for differnce lags
    print (coin,table,metric)
    #df_price = pd.DataFrame(df_price.price.ethereum)
    query = query_construct(table,coin,metric)
    df = pd.read_sql(query, make_eng())
    if df.empty:
        return None
    df = df.set_index('as_of')
    df = df.loc[:, ['price', 'm_value']]
    df = df.rename(columns={'m_value':'today'})
    
    for i in shift:
        df['lag_' + str(i)] = df.today.shift(i)
    
    
    corr_matrix_array = []
    
    for sp in range(split):
        
        bite = int(len(df)/split)
        
        df_tmp = df.iloc[sp*bite:(sp+1)*bite,:]
        
        corr_matrix = np.log(df_tmp).diff(periods=difference).rolling(rolling_win).corr()
        corr_matrix = corr_matrix.reset_index()
        corr_matrix = corr_matrix[corr_matrix['level_1']=='price'].set_index('as_of')
        corr_matrix = corr_matrix.drop('price', axis=1)
        corr_matrix.name = str(coin) + '_' + str(table) + '_' + str(metric) + '_' + str(corr_matrix.index[-1])
       
        corr_matrix_array.append(corr_matrix)

    return corr_matrix_array


if __name__ == '__main__':
      
    query_list = [['bitcoin', 'active_count'], ['ethereum', 'active_count'], ['litecoin','active_count'], ['uma','active_count']]
    coin_list = ['USDT', 'DAI','UNI', 'BTC', 'ETH', 'LTC','UMA']
    table_list = [['glassnode_addresses', 'active_count'], ['glassnode_addresses', 'count'],['glassnode_addresses', 'sending_count'],['glassnode_addresses', 'receiving_count'], ['glassnode_addresses', 'new_non_zero_count'],
                  ['glassnode_fees','volume_sum'],['glassnode_fees','gas_price_median'],['glassnode_fees','gas_price_mean']]
    
    
    diff_days=30
    for i in coin_list:
        with PdfPages('graphs/corr_' + str(i) +'.pdf') as pdf:
            for j in table_list:
                corr_matrix_array = calc_corr(i, j[0],j[1], difference=diff_days)
                if corr_matrix_array is not None:
                    for k in corr_matrix_array:
                        k.mean().plot()
                        plt.title('corr ' + k.name + ' V lag, diff ' + str(diff_days) + ' days' , fontsize=8)
                        pdf.savefig(dpi=300)
                        plt.close()
                    corr_portfolio(i, j[0],j[1])